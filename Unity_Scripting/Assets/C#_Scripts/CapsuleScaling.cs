using UnityEngine;

public class CapsuleScaling : MonoBehaviour
{
    [SerializeField]
    private Vector3 axes;
    public float scaleUnits;



    void Update()
    {

        axes = ScriptCapsuleMovement.ClampVector3(axes);
        transform.localScale += axes * (scaleUnits * Time.deltaTime);


    }
}
